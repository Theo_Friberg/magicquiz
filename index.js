
/*

Copyright (c) 2015 Théo Friberg

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

document.addEventListener('DOMContentLoaded', function() { // JavaScript setup on page load

  var startButton = document.getElementById('start server'); // Get the start button
    
  document.getElementById('files').addEventListener('change', function (evt) { // Enable the button and read the file when a file is selected
    var input = event.target; // Get the input
    var reader = new FileReader(); // Initialize the reader.
    reader.onload = function(){ // Code ran when the file is read.
      var text = reader.result.replace(/\r/gm, ''); // Get the text and store it into the background.js.
      chrome.runtime.getBackgroundPage(function(bgpage) {
        bgpage.quiz = text;
      });
      document.getElementById('start server').disabled = false; // Enable the button to start the server
    };
    reader.readAsText(input.files[0]); // Read the file
  }, false);
  
  startButton.addEventListener('click', function() { // Open the serversetup.html page when the Start-button is started.
  
    chrome.app.window.create("scoreboard.html", { // Open the scoreboard.html -page
      "outerBounds": {
        "width": 600,
        "height": 700,
        "left": 200,
        "top": 200
      }
    });
    
    chrome.app.window.create("serversetup.html", { // Open the serversetup.html -page
      "outerBounds": {
        "width": 600,
        "height": 700,
        "left": 250,
        "top": 250
      }
    });
  });
});
