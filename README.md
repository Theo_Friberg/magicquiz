# README #

## What is this? ##

MagicQuiz is a Chrome app for organizing quizzes or quick exams over a local network. It is **not** engineered towards security. It is optimized for **quick setup** on school computers **without special rights** and regardless of the underlying OS. It is for the students to evaluate their knowledge, **not** for official exams.


### Legalese and technobabble###

MagicQuiz is copyright Théo Friberg under the MIT lisence. (See all the way down.)

* MagicQuiz
* V. 0.67

## How do I get set up? ##

1. Download the .crx extension from the "Downloads" on the left.
2. Go into Google Chrome / Chromium and type ```chrome://extensions``` into the Omnibar.
3. Drag and drop the downloaded extension into Chrome.

You have now installed MagicQuiz. Launch it using your environment's default launcher.

## Who do I talk to? ##

Contact me at theo.friberg@gmail.com

### MIT lisence ###

This app contains files from other authors. These include the files assets/Lora-Regular.ttf and assets/Muli-Regular.ttf and the file qrcode.min.js. The two first files are from Google fonts under the SIL Open Font Licence 1.1 by (in order) Cyreal and Vernon Adams. The file qrcode.min.js is from the QRCodeJS.js library by various authors. See [here](https://github.com/davidshimjs/qrcodejs) for the library's GitHub page.

Copyright (c) 2015 Théo Friberg

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.