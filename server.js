
  
/*
  
Copyright (c) 2015 Théo Friberg

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,´
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  
*/

// Callback for accessing the address field of the page

function setIP(ip){
  document.getElementById("address").innerHTML = ip;
}

// Shorten url using lyli.fi (Service created by Felix Bade)

function lyli(url){
  post("http://api.lyli.fi", '{"url": "'+url+'"}', "application/json");
}

// Post data to a remote site (only used for shortening links)

function post(url, params, contentType) {
  var http = new XMLHttpRequest();
  http.open("POST", url, true);
  http.setRequestHeader("Content-Type", contentType);
  http.onreadystatechange = function() { //Call a function when the state changes.
      if(http.readyState == 4) {
	      setIP(JSON.parse(http.responseText)["short-url"]);
	  }
  }
  http.send(params);
}

var quizMarkup = ""; // Store the question file locally

chrome.runtime.getBackgroundPage(function(bgpage) {
  quizMarkup = bgpage.quiz;
});

/*

 RTC -magic to get the local ip ----------------------------------------------------------------

*/

// NOTE: window.RTCPeerConnection is "not a constructor" in FF22/23
var RTCPeerConnection = /*window.RTCPeerConnection ||*/ window.webkitRTCPeerConnection || window.mozRTCPeerConnection;
if (RTCPeerConnection) (function () {
    var rtc = new RTCPeerConnection({iceServers:[]});
    if (1 || window.mozRTCPeerConnection) {      // FF [and now Chrome!] needs a channel/stream to proceed
        rtc.createDataChannel('', {reliable:false});
    }

    rtc.onicecandidate = function (evt) {
        // convert the candidate to SDP so we can run it through our general parser
        // see https://twitter.com/lancestout/status/525796175425720320 for details
        if (evt.candidate) grepSDP("a="+evt.candidate.candidate);
    };
    rtc.createOffer(function (offerDesc) {
        grepSDP(offerDesc.sdp);
        rtc.setLocalDescription(offerDesc);
    }, function (e) { console.warn("offer failed", e); });


    var addrs = Object.create(null);
    addrs["0.0.0.0"] = false;
    function updateDisplay(newAddr) {
        if (newAddr in addrs) return;
        else addrs[newAddr] = true;
        var displayAddrs = Object.keys(addrs).filter(function (k) { return addrs[k]; });
        var a = "";
        if(displayAddrs.length == 1){
          a = displayAddrs[0];

          new QRCode(document.getElementById("qr"), a+":8080");
          setIP("http://"+a+":8080"); // Shown on screen before lyli.fi responds, eg. when access to the internet is unavailable
          lyli("http://"+a+":8080");
        }else if(displayAddrs.length > 1){
          a = displayAddrs[0];
          new QRCode(document.getElementById("qr"), a+":8080");
          document.getElementById("error").innerHTML = " Warning: multiple addresses detected.";
          setIP("http://"+a+":8080"); // Shown on screen before lyli.fi responds, eg. when access to the internet is unavailable
          lyli("http://"+a+":8080");
        }else{
          document.getElementById("error").innerHTML = " Error: no address detected.";
        }
        console.log(a)
        kickstartServer(a);
    }

    function grepSDP(sdp) {
        var hosts = [];
        sdp.split('\r\n').forEach(function (line) { // c.f. http://tools.ietf.org/html/rfc4566#page-39
            if (~line.indexOf("a=candidate")) {     // http://tools.ietf.org/html/rfc4566#section-5.13
                var parts = line.split(' '),        // http://tools.ietf.org/html/rfc5245#section-15.1
                    addr = parts[4],
                    type = parts[7];
                if (type === 'host') updateDisplay(addr);
            } else if (~line.indexOf("c=")) {       // http://tools.ietf.org/html/rfc4566#section-5.7
                var parts = line.split(' '),
                    addr = parts[2];
                updateDisplay(addr);
            }
        });
    }
})(); else {
    document.getElementById('list').innerHTML = "<code>ifconfig | grep inet | grep -v inet6 | cut -d\" \" -f2 | tail -n1</code>";
    document.getElementById('list').nextSibling.textContent = "In Chrome and Firefox your IP should display automatically, by the power of WebRTCskull.";
}

// Convenience functions to convert data

function t2ab(str /* String */) {
    var buffer = new ArrayBuffer(str.length);
    var view = new DataView(buffer);
    for(var i = 0, l = str.length; i < l; i++) {
      view.setInt8(i, str.charAt(i).charCodeAt());
    }
    return buffer;
}

function ab2t(buffer /* ArrayBuffer */) {
  var arr = new Int8Array(buffer);
  var str = "";
  for(var i = 0, l = arr.length; i < l; i++) {
    str += String.fromCharCode.call(this, arr[i]);
  }
  return str;
}

var decodeHtmlEntity = function(str) {
  return str.replace(/&#(\d+);/g, function(match, dec) {
    return String.fromCharCode(dec);
  });
};

var encodeHtmlEntity = function(str) {
  var buf = [];
  for (var i=str.length-1;i>=0;i--) {
    buf.unshift(['&#', str[i].charCodeAt(), ';'].join(''));
  }
  return buf.join('');
};

// Clean an array of empty elements

function clean(arr){
  var newArray = [];
  var i = 0;
  while(i < arr.length){
    if(arr[i] !== ""){
      newArray.push(arr[i]);
    }
    i += 1;
  }
  return newArray;
}

// Request header

var RESPHEAD = [
  "HTTP/1.1 200 OK",
  "Server: chrome24",
  "Content-Length: {%len%}",
  "Connection: Close",
  "Content-Type: text/html; charset=utf-8"
];

RESPHEAD = RESPHEAD.join("\r\n")+"\r\n\r\n";

// Transform markup into functional quiz

function stringifyMarkup(markup){
  var title = markup.split("\n")[0];
  var result = `<!doctype html>
<head>
<meta charset="UTF-8">
`
  result += "<title>"+title+"</title>"
  result += `<link href='https://fonts.googleapis.com/css?family=Lora|Muli' rel='stylesheet' type='text/css'>
<style>
body{
  text-align: center;
  position: relative;
}

h1{
  margin-top: 1em;
  font-size: 4em;
  font-family: Lora;
}

p{
  font-size: 2.5em;
  margin-top: 5em;
  font-family: Muli;
  width: 80%;
  margin-left: auto;
  margin-right: auto;
}
.button, button{
  font-size: 2em;
  margin-top: 1.5em;
  background: linear-gradient(to bottom, #49a1e5 1%, #1e5799 100%);
  border-style: none;
  color: white;
  padding-left: 0.5em;
  padding-right: 0.5em;
  padding-top: 0.2em;
  padding-bottom: 0.2em;
  border-radius: 0.3em;
  box-shadow: 0px 2px 14px 0px rgba(0,0,0,0.26);
  font-family: Muli;
}

.button:hover, button:hover {
  background: linear-gradient(to bottom, #7db9e8 0%,#1e5799 99%);
}

.button:active, button:active {
  background: linear-gradient(to bottom, #2d95e5 0%,#004799 100%);
}

.button:focus, button:focus {
  outline: 0;
}

input[type=radio]{
    border: 0px;
    height: 2em;
    margin-left: 5em;
}

.name{
    margin-left: 5em;
    font-size: 3em;
}

label {
  font-family: Lora;
  font-size: 1.2em;
  margin: 0.5em;
}

</style>
</head>
<body>
<h1>`+title+`</h1>
<form style="text-align: left; font-size: 1.2em;" action="/submit" method="get">`
var questions = markup.split("\n\n"); // Split the input file into individual questions
var i = 1; // Iterator inited to 1 to skip the title
var dataIndex = 1; // Index of the field
while(i < questions.length){
  var ii = 1; // Iterator inited to 1 to skip the question's title
  if(1 < clean(questions[i].split("\n")).length){ // Are we dealing with a multiple choice question?
  result += "<p style=\"margin-bottom:0.5em; margin-top:2em;\">"+encodeHtmlEntity(questions[i].split("\n")[0])+"</p>";
  while(ii < questions[i].split("\n").length){
    var proposition = questions[i].split("\n")[ii];
    if(proposition.slice(2, proposition.length) !== ""){
      var answer = proposition.slice(1, proposition.length);
      if(answer.length > 0 && answer[0] == " "){ // Optional whitespace
        answer = answer.slice(1, answer.length)
      }
      result += "<input type=\"radio\" name=\"Q"+dataIndex.toString()+"\" id value=\"Q"+dataIndex.toString()+"-"+ii.toString()+"\" value=\"Q"+i.toString()+"-"+ii.toString()+"\"><label for=\"Q"+dataIndex.toString()+"-"+ii.toString()+"\">" + encodeHtmlEntity(answer) + "<br/></label>";

    }
    ii += 1;
  }
        dataIndex += 1
  }else if(questions[i].indexOf("_") > -1){ // We are dealing with a free-form question
    var iii = 0;
    var bits = clean(questions[i].split("\n"))[0].split("_");
    result += "<p style=\"margin-bottom:0.5em; margin-top:2em;\">";
    while(iii < bits.length){
      if(iii % 2 == 1){
        result +="<input type=\"text\" name=\"Q"+dataIndex.toString()+"\" style=\"font-size: 0.7em; width: "+bits[iii].length.toString()+"em;\">";
        dataIndex += 1
      }else{
        result += encodeHtmlEntity(bits[iii]);
      }
      iii += 1;
    }
    result += "</p>";
  }else{
    result += "<p style=\"margin-bottom:0.5em; margin-top:2em;\">"+encodeHtmlEntity(questions[i])+"</p>";
  }
  i += 1;
}

result += `
<p style="margin-bottom:0.5em; margin-top:2em;">Name</p>
<input type="text" name="nickname" class="name"/>
<input type="submit" value="Submit" class="button" style="display: block; margin-left: auto; margin-right: auto; margin-bottom: 1em;">
</form>

</body>
</html>`
  return result.replace("\n", "\r\n"); // Replace line endings
}

// Combine the header with the rest of the data

var response = function(str){
  var len = str.length;
  return RESPHEAD.replace("{%len%}", len)+str;
}

// Extract the right answers

function extractRight() {
  var questions = quizMarkup.split("\n\n"); // Split the input file into questions
  var i = 1; // Init the iterator to 1 to skip the title
  var result = [];
  while(i < questions.length){
    var ii = 1; // Skip the question
    var q = []; // The data for this question
    if(1 < clean(questions[i].split("\n")).length){ // If we are dealing with a multiple choice question
    while(ii < questions[i].split("\n").length){
      var proposition = questions[i].split("\n")[ii];
      if(proposition.slice(2, proposition.length) !== ""){ // Skip empty choices
        if(proposition[0] === "*"){ // Remember if the answer was right
          q.push(true);
        }else{
          q.push(false);
        }
      }
      ii += 1;
    }
    result.push(q); // Push the data for that question into the answers
    }else if(questions[i].indexOf("_") > -1){ // If we are dealing with a free-form question
      var bits = clean(questions[i].split("\n"))[0].split("_") // Split it up into bits; every second bit will be an answer
      var iii = 0;
      while(iii < bits.length){ // Push every second item from the bits variable
      if(iii % 2 == 1){
        result.push([bits[iii]]);
      }
      iii += 1;
    }
    }

    i += 1;
}
return result;
}

// Return an array of booleans (true -> right answer; false -> wrong answer)

function extractGivenAnswers(query){
  if(query.indexOf("?") > -1){
      if(query.indexOf("Q") > -1){ // The query looks remotely right
        var i = 0;
        var rightAnswers = extractRight();
        var givenAnswers = [];
        while(i < rightAnswers.length){ // We set up so that every answer counts as wrong. Then we correct as we go
          givenAnswers.push(false);
          i += 1;
        }
        i = 0;
        var queryChunked = query.split("?")[1].split("&");
        queryChunked = queryChunked.slice(0, queryChunked.length - 1); // Get the give answers
        while(i < queryChunked.length){ // Iterate over the given answers
          if(queryChunked[i].length > 0 && queryChunked[i][0] === "Q" && queryChunked[i].indexOf("=") > -1){ // Is the formatting remotely right?
            var q = parseInt(queryChunked[i].slice(1, queryChunked[i].indexOf("="))); // Get the number of the question
            if(typeof q === "number"){ // If we got a number (user didn't give crappy data)
              q = q - 1; // Decrement it (we are using it as an index)
              if(q < givenAnswers.length){ // If it refers to a question actually in the quizz.
                var proposition = queryChunked[i].split("=")[1]; // Get the answer itself
                if(proposition.length > 0){ // If it isn't empty
                  if(typeof rightAnswers[q][0] === "string"){ // If we want a string
                    if(decodeURIComponent(proposition.replace("+", " ")) === rightAnswers[q][0]){ // Test if it was right
                      givenAnswers[q] = true;
                    }else{ // Avoid manipulation by setting a value repeatedly (eg. Q1=quess&Q1=other+guess)
                      givenAnswers[q] = false;
                    }
                  }else if(typeof rightAnswers[q][0] === "boolean" && proposition.indexOf("-") > -1 && proposition.split("-").length > 1){ // If it is a multiple choice question
                    var answerIndex = parseInt(proposition.split("-")[1]) // The index of the right answer according to the user
                    if(typeof answerIndex === "number"){ // If we got a number
                      answerIndex -= 1; // It is an index starting from 0
                      if(answerIndex > -1 && answerIndex < rightAnswers[q].length){ // If it is nothing too crazy
                        if(rightAnswers[q][answerIndex]){ // Is it right?
                          givenAnswers[q] = true;
                        }else{ // Avoid manipulation by setting a value repeatedly (eg. Q1=Q1-1&Q1=Q1-2)
                          givenAnswers[q] = false;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
          i += 1
        }
        return givenAnswers;
      }else{
        "No answers"
      }
  }else{
    return "No data"
  }
}

var rtw = function(info) { // Function called when a socket is accepted
  
  chrome.sockets.tcp.onReceive.addListener(function(recvInfo) { // React to request
    if (recvInfo.socketId != info.clientSocketId) // If something went screwy in the socket layer
      return;
    var query = ab2t(recvInfo.data).split("\n")[0].split(" ")[1]; // Extract the query from the http header 
    if(query.split("?")[0] === "/submit"){ // Is the client submiting a quizz?
      var name = "&lt;no name given&gt;";
      var veracity = ""; // Variables to hold the textual representation of the student's score
      var score = "";
      var i = 0;
      var answers = extractGivenAnswers(query)
      var rightAmount = 0;
      if(typeof answers !== "string"){
        while(i < answers.length){
          if(answers[i]){
            veracity += "<br/><span style=\"color: green; text-shadow: 0px 0px 7px rgba(150, 150, 150, 1);\">&#x2713;</span>"
            rightAmount += 1
          }else{
            veracity += "<br/><span style=\"color: red; text-shadow: 0px 0px 7px rgba(150, 150, 150, 1);\">&#x2717;</span>"
          }
          i += 1;
        }
      }
      
      if(query.split("?").length > 1 && query.indexOf("nickname=") > -1 && query.split("nickname=").length > 0){
        name = encodeHtmlEntity(decodeURIComponent(query.split("nickname=")[1].split("&")[0].replace("+", " ")));
      }
      if(name === ""){
        name = "&lt;no name given&gt;";
      }
      score = rightAmount.toString() + " / " + i.toString();
      chrome.runtime.getBackgroundPage(function(bgpage) {
                console.log("'"+name+"'")
        if(i === rightAmount){
          bgpage.updateScore("<p style=\"margin-top: 1em; margin-bottom: 1em;\">"+name+": "+score+" <span style=\"color: green; text-shadow: 0px 0px 7px rgba(150, 150, 150, 1);\">&#x2713;</span></p>")
        }else{
          bgpage.updateScore("<p style=\"margin-top: 1em; margin-bottom: 1em;\">"+name+": "+score+"</p>")
        }
      
      })
      chrome.sockets.tcp.send(info.clientSocketId, t2ab(response(`
<!doctype html>
<head>
<meta charset="UTF-8">
<title>Submit</title>
<link href='https://fonts.googleapis.com/css?family=Lora|Muli' rel='stylesheet' type='text/css'>
<style>
body{
  text-align: center;
  position: relative;
}

h1{
  margin-top: 1em;
  font-size: 4em;
  font-family: Lora;
}

p{
  font-size: 2.5em;
  margin-top: 5em;
  font-family: Muli;
  width: 80%;
  margin-left: auto;
  margin-right: auto;
}
.button, button{
  font-size: 2em;
  margin-top: 1.5em;
  background: linear-gradient(to bottom, #49a1e5 1%, #1e5799 100%);
  border-style: none;
  color: white;
  padding-left: 0.5em;
  padding-right: 0.5em;
  padding-top: 0.2em;
  padding-bottom: 0.2em;
  border-radius: 0.3em;
  box-shadow: 0px 2px 14px 0px rgba(0,0,0,0.26);
  font-family: Muli;
}

.button:hover, button:hover {
  background: linear-gradient(to bottom, #7db9e8 0%,#1e5799 99%);
}

.button:active, button:active {
  background: linear-gradient(to bottom, #2d95e5 0%,#004799 100%);
}

.button:focus, button:focus {
  outline: 0;
}

input[type=radio]{
    border: 0px;
    height: 2em;
    margin-left: 5em;
}

label {
  font-family: Lora;
  font-size: 1.2em;
  margin: 0.5em;
}

</style>
</head>
<body>
<h1>Test</h1>
<p>Results: `+score+veracity+`</p>
</body>
</html>`)),
    function(resultCode) {
      console.log("Data sent to new TCP client connection.")
  });
      
    }else{
      // A new TCP connection has been established.
    console.log(query)
  chrome.sockets.tcp.send(info.clientSocketId, t2ab(response(stringifyMarkup(quizMarkup))),
    function(resultCode) {
      console.log("Data sent to new TCP client connection.")
  });
    
    }
    // recvInfo.data is an arrayBuffer.
  });
  chrome.sockets.tcp.setPaused(info.clientSocketId, false);
    
}

function kickstartServer(address){
chrome.sockets.tcpServer.create({}, function(e){
  var s = e;
  chrome.sockets.tcpServer.listen(s.socketId, address, 8080, 10, function(e){
    chrome.sockets.tcpServer.getInfo(s.socketId, function(e){
      console.log("Local web server's URL => http://localhost:"+e.localPort+"/"); // you can check listen port :)
    });
    var accept_ = function(sid){
      chrome.sockets.tcpServer.onAccept.addListener(function(e){
        rtw(e);
      });
    }
    accept_(s.socketId);
  });
});
}